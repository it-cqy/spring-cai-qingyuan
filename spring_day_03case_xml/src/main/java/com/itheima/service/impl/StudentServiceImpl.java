package com.itheima.service.impl;

import com.itheima.bean.Student;
import com.itheima.mapper.StudentMapper;
import com.itheima.service.StudentService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.io.InputStream;
import java.util.List;

//注解配置bean对象 id=ioc容器中唯一标识 studentService
@Service("studentService")
public class StudentServiceImpl implements StudentService {
    //注入mapper代理对象   自动装配
    //注入依赖 自动装配  Qualifier @Qualifier("StudentMapper")
    //自动转配
    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<Student> selectAll() throws IOException {
        return studentMapper.selectAll();
    }

    @Override
    public Student selectById(Integer id) throws IOException {
        return studentMapper.selectById(id);
    }

    @Override
    public Integer insert(Student stu) throws IOException {
        return studentMapper.insert(stu);
    }

    @Override
    public Integer update(Student stu) throws IOException {
        return studentMapper.update(stu);
    }

    @Override
    public Integer delete(Integer id) throws IOException {
        return studentMapper.delete(id);
    }
}
