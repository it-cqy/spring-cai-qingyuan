package com.itheima.service;


import com.itheima.bean.Student;
import com.itheima.springConfig.SpringConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
 //@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class StudentServiceTest {
    //依赖注入

    @Autowired  //自动装配  Qualifier
    private  StudentService studentService;


    @Test
    public void test () throws IOException {
        List<Student> students = studentService.selectAll();
        students.forEach(student -> System.out.println(student));
        Assert.assertEquals(3,students.size());
    }


}
