package com.itheima.aop;

import org.aspectj.lang.ProceedingJoinPoint;
@SuppressWarnings("all")
public class Advice {


    public Object around(ProceedingJoinPoint pjp) {
        Object value = null;
        try {
            //1 记录开始时间
            long start = System.currentTimeMillis();
            //2 执行目标方法
            value = pjp.proceed();
            //3 记录结束时间
            long end = System.currentTimeMillis();
            //获取方法名称
            String methodName = pjp.getSignature().getName();
            //4 打印结果
            System.out.println("执行" + methodName + "方法共耗时：" + (end - start) + "毫秒");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return value;
    }
}
