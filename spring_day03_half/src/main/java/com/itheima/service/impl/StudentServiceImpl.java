package com.itheima.service.impl;

import com.itheima.bean.Student;
import com.itheima.mapper.StudentMapper;
import com.itheima.service.StudentService;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service("studentService")   //注解配置bean对象 id=ioc容器中唯一标识 studentService
public class StudentServiceImpl implements StudentService {
    //注入mapper代理对象   自动装配
    //注入依赖 自动装配  Qualifier @Qualifier("StudentMapper")
    @Autowired
    private StudentMapper mapper;

    @Override
    public List<Student> selectAll() throws IOException {
        // List<Student> list = mapper.selectAll();


        return mapper.selectAll();
    }

    @Override
    public Student selectById(Integer id) throws IOException {
        //1、加载核心配置文件
       /* InputStream is = Resources.getResourceAsStream("MybatisConfig.xml");
        //2、获取工厂对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        //3、获取SqlSession核心对象
        SqlSession sqlSession = factory.openSession(true);

        //4、获取接口的实现类代理对象，执行操作

        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        //执行代理对象的方法
        Student student = mapper.selectById(id);   //底层使用sqlSession.selectOne()方法
        //6、释放资源
        sqlSession.close();*/
        Student student = mapper.selectById(id);
        return mapper.selectById(id);
    }

    @Override
    public Integer insert(Student stu) throws IOException {
       /* //1、加载核心配置文件
        InputStream is = Resources.getResourceAsStream("MybatisConfig.xml");
        //2、获取工厂对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        //3、获取SqlSession核心对象
        SqlSession sqlSession = factory.openSession(true);

        //4、获取接口的实现类代理对象，执行操作

        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        //执行代理对象的方法
        Integer count = mapper.insert(stu);   //底层使用sqlSession.selectOne()方法

        //6、释放资源
        sqlSession.close();*/
        return mapper.insert(stu);
    }

    @Override
    public Integer update(Student stu) throws IOException {
       /* //1、加载核心配置文件
        InputStream is = Resources.getResourceAsStream("MybatisConfig.xml");
        //2、获取工厂对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        //3、获取SqlSession核心对象
        SqlSession sqlSession = factory.openSession(true);

        //4、获取接口的实现类代理对象，执行操作

        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        //执行代理对象的方法
        Integer count = mapper.update(stu);   //底层使用sqlSession.selectOne()方法

        //6、释放资源
        sqlSession.close();*/
        return mapper.update(stu);
    }

    @Override
    public Integer delete(Integer id) throws IOException {
       /* //1、加载核心配置文件
        InputStream is = Resources.getResourceAsStream("MybatisConfig.xml");
        //2、获取工厂对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        //3、获取SqlSession核心对象
        SqlSession sqlSession = factory.openSession(true);
        //4、获取接口的实现类代理对象，执行操作
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        Integer count = mapper.delete(id);
        //5、处理/打印结果
        System.out.println("count = " + count);
        //6、释放资源
        sqlSession.close();*/
        return mapper.delete(id);
    }
}
