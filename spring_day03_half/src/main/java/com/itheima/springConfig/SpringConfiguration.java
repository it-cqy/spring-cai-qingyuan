package com.itheima.springConfig;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration    //表示该类是spring配置类
@ComponentScan("com.itheima")  //开启spring组件扫描
@PropertySource("classpath:jdbc.properties")  //引入属性文件
@Import({JdbcConfig.class, MybatisConfig.class})    //引入spring子配置 第三方配置
public class SpringConfiguration {

}
