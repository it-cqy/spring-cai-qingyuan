package com.itheima.dataSource;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration  //用在类上，表示该类是一个spring配置类，代替applicationContext.xml配置文件

//<!--<import resource="classpath:xxxxx.xml"/>
@Import({JdbcConfig.class})


// <!--【第一步】：引入外部的属性文件-->
//    <context:property-placeholder location="classpath:jdbc.properties"/>
@PropertySource("classpath:jdbc.properties")

//<!--开启spring 注解扫描-->
//    <context:component-scan base-package="com.itheima"/>
@ComponentScan("com.itheima")
public class SpringConfiguration {
}
