package com.itheima.service.impl;

import com.itheima.dao.UserDao;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Service//("userService")
public class UserServiceImpl implements UserService {
     @Value("你这就是上课完全没听讲")
    private String name;
     @Value("25")
    private int age;

    @Autowired
    private UserDao userDao;


    @Override
    public void save() {
        System.out.println("UserServiceImpl save is running..."+name+"..."+age);
        userDao.save();
    }
}