package com.itheima.deletePage;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@SuppressWarnings("all")
public class JdbcConfig {

    //jdbc.driver=com.mysql.jdbc.Driver
    //jdbc.url=jdbc:mysql://localhost:3306/heima_mm
    //jdbc.username=root
    //jdbc.password=root
    @Value("${jdbc.driver}")
    private String driverClassName;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.use}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    //使用注解配置第三方类
    @Bean("dataSource")
    public DataSource dataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driverClassName);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        return druidDataSource;
    }


}
