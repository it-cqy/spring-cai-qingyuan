package com.itheima.deletePage;

import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

public class MyBatisXConfig {

    //使用注解配置第三方文件
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        //设置参数
        factoryBean.setDataSource(dataSource);
        //别名
        factoryBean.setTypeAliasesPackage("com.itheima.bean");
        //返回参数
        return factoryBean;
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapper = new MapperScannerConfigurer();
        //设置参数 生产代理对象
        mapper.setBasePackage("com.itheima.mapper");
        //返回参数
        return mapper;
    }
}
