package com.itheima.deletePage;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration //该类是spring配置的类
@ComponentScan("com.itheima")   //注解 开启spring扫包
@PropertySource("jdbc.properties")  //引入属性文件
@Import({JdbcConfig.class, MyBatisXConfig.class}) //引入子配置
public class SpringConfiguration {
}
