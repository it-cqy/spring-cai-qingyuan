package com.itheima.springConfig;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;
@SuppressWarnings("all")
public class JdbcConfig {


    @Value("${jdbc.driver}")
    private String driverClassName;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    // 使用注解配置第三方类
    @Bean("dataSource")
    public DataSource dataSource() {
        //创建连接池对象
        DruidDataSource dataSource = new DruidDataSource();
        // 设置连接参数
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        //返回连接池对象
        return dataSource;
    }

}
