package com.itheima.springConfig;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.context.annotation.Bean;
import javax.sql.DataSource;

public class MybatisConfig {

    //使用注解配置第三方类
    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) {
        //1 创建SqlSessionFactoryBean对象
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        //设置参数
        factoryBean.setDataSource(dataSource);
        //别名
        factoryBean.setTypeAliasesPackage("com.itheima.bean");
        //3 返回SqlSessionFactoryBean
        return factoryBean;
    }

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        //1 创建MapperScannerConfigurer对象
        MapperScannerConfigurer msc = new MapperScannerConfigurer();
        //2 设置参数
        msc.setBasePackage("com.itheima.mapper");
        //3 返回MapperScannerConfigurer对象
        return msc;
    }
}

