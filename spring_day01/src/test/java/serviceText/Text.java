package serviceText;

import com.alibaba.druid.pool.DruidAbstractDataSource;
import com.alibaba.druid.pool.DruidDataSource;
import com.itheima.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class Text {

    @Test
    public void text() {
        //获取ApplicationContext对象
        ApplicationContext app = new ClassPathXmlApplicationContext("ApplicationContext.xml");

        //2 从容器中根据id值获取对象
        UserService userService = app.getBean("userService", UserService.class);

        //调用方法
        userService.save();

    }

    @Test
    public void text01() throws SQLException {
        //获取ApplicationContext对象
        ApplicationContext app = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        //2 从容器中根据id值获取对象
        DataSource userService = app.getBean("dataSource", DruidDataSource.class);
        //调用方法
        Connection connection = userService.getConnection();
        System.out.println("connection = " + connection);

    }


}
