package com.itheima.service.Impl;

import com.itheima.service.UserService;
import com.itheima.service.dao.UserDao;

public class UserServiceImpl implements UserService {

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserDao userDao, String name, int age) {
        this.userDao = userDao;
        this.name = name;
        this.age = age;
    }

    //注入依赖  对象
    private UserDao userDao;

    //一般类型
    private String  name;
    private  int   age;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }



    @Override
    public void save() {
        System.out.println("520 key!"+name+" "+age);
        userDao.play();
    }
}
