package com.itheima.test;


import com.itheima.bean.Student;
import com.itheima.service.StudentService;
import com.itheima.service.impl.StudentServiceImpl;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

public class StudentTest {


    @Test
    public void test3() throws IOException {
        //创建Spring容器对象
        ClassPathXmlApplicationContext app = new ClassPathXmlApplicationContext("applicationContext.xml");
        //获取service对象
        StudentService studentService = app.getBean("studentService", StudentService.class);
        //执行操作，调用service的方法
        List<Student> students = studentService.selectAll();
        //遍历
        students.forEach(stu-> System.out.println(stu));
    }

}
